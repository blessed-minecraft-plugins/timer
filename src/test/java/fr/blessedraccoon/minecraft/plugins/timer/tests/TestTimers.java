package fr.blessedraccoon.minecraft.plugins.timer.tests;

import fr.blessedraccoon.minecraft.plugins.timer.timing.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestTimers {
	
	private TimerManager timerManager;
	
	private static StandardTimer FIRST;
	private static StandardTimer SECOND;
	
	@Before
	public void init() {
		this.timerManager = new TimerManager(null);
		try {
			timerManager.createTimer("test1");
			timerManager.createTimer("test2");
			FIRST = timerManager.getTimer("test1");
			SECOND = timerManager.getTimer("test2");
		} catch (TimerInvalidNameException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCreate() {
		assertEquals(2, this.timerManager.getTimers().size());
		assertEquals(FIRST, this.timerManager.getTimers().get(0));
		assertEquals(SECOND, this.timerManager.getTimers().get(1));
	}

	@Test(expected = TimerInvalidNameException.class)
	public void testCreateSameName() throws TimerInvalidNameException {
		timerManager.createTimer("test2");
	}
	
	@Test
	public void testGetTimer() {
		assertEquals(FIRST, this.timerManager.getTimer("test1"));
		assertEquals(SECOND, this.timerManager.getTimer("test2"));
	}

	
	@Test
	public void testAddTime() throws TimeOutOfBoundsException {
		this.timerManager.getTimer("test1").addTime(7);
		this.timerManager.getTimer("test2").addTime(643);
		assertEquals(7, this.timerManager.getTimer("test1").getTime());
		assertEquals(643, this.timerManager.getTimer("test2").getTime());
	}
	
	@Test
	public void testToString() throws TimeOutOfBoundsException {
		this.timerManager.getTimer("test1").addTime(7);
		this.timerManager.getTimer("test2").addTime(643);
		assertEquals("00:00:07", this.timerManager.getTimer("test1").toString());
		assertEquals("00:10:43", this.timerManager.getTimer("test2").toString());
	}
	
	@Test
	public void testHighToString() throws TimeOutOfBoundsException {
		this.timerManager.getTimer("test1").addTime(86402);
		assertEquals("1 day, 00:00:02", this.timerManager.getTimer("test1").toString());
		this.timerManager.getTimer("test1").addTime(86402);
		assertEquals("2 days, 00:00:04", this.timerManager.getTimer("test1").toString());
		
		
		this.timerManager.getTimer("test2").addTime(31536002);
		assertEquals("1 year, 00:00:02", this.timerManager.getTimer("test2").toString());
		this.timerManager.getTimer("test2").addTime(86400 * 2);
		assertEquals("1 year, 2 days, 00:00:02", this.timerManager.getTimer("test2").toString());
		this.timerManager.getTimer("test2").addTime(31536000);
		assertEquals("2 years, 2 days, 00:00:02", this.timerManager.getTimer("test2").toString());
		
		this.timerManager.getTimer("test2").reset();
		this.timerManager.getTimer("test2").addTime(1_000_000_000_000_000_000L);
		assertEquals("31709791983 years, 279 days, 01:46:40", this.timerManager.getTimer("test2").toString());
	}
	
	@Test
	public void testDisplay() {
		assertNull(this.timerManager.getVisible());
		this.timerManager.setVisible(this.timerManager.getTimer("test1"));
		assertEquals(FIRST, this.timerManager.getVisible());
		this.timerManager.setVisible(this.timerManager.getTimer("test2"));
		assertEquals(SECOND, this.timerManager.getVisible());
	}
	
	@Test
	public void testRemove() throws TimerInvalidNameException {
		this.timerManager.removeTimer("test1");
		assertEquals(SECOND, this.timerManager.getTimer("test2"));
		assertNull(this.timerManager.getTimer("test1"));
		this.timerManager.removeTimer("test2");
		assertNull(this.timerManager.getTimer("test2"));
	}
	
	@Test(expected = TimerInvalidNameException.class)
	public void testRemoveNotExist() throws TimerInvalidNameException {
		timerManager.removeTimer("test3");
	}
	
	@Test
	public void testUpdateRunning() {
		StandardTimer timer = this.timerManager.getTimer("test1");
		timer.setRunning(true);
		timer.update();
		timer.update();
		timer.update();
		assertEquals(3, this.timerManager.getTimer("test1").getTime());
	}

	@Test
	public void testUpdateNotRunning() {
		StandardTimer timer = this.timerManager.getTimer("test1");
		timer.setRunning(false);
		timer.update();
		assertEquals(0, this.timerManager.getTimer("test1").getTime());
	}
	
	@Test
	public void testListener() {
		FIRST.setRunning(true);
		TimeListener tl = new TimeListener() {

			@Override
			public int getTriggerTime() {
				return 2;
			}

			@Override
			public void trigger(StandardTimer tm, long time) {
				assertEquals(2, time);
				tm.setName("test3");
			}
		};
		this.timerManager.getTimer("test1").addTrigger(tl);
		this.timerManager.getTimer("test1").update();
		assertEquals(FIRST, this.timerManager.getTimer("test1"));
		this.timerManager.getTimer("test1").update();
		assertEquals(FIRST, this.timerManager.getTimer("test3"));
		assertNull(this.timerManager.getTimer("test1"));
	}
	
	@Test
	public void testCountdown() throws TimeOutOfBoundsException {
		FIRST.setRunning(true);
		FIRST.setCountMethod(StandardTimer.COUNTDOWN_METHOD);
		FIRST.addTime(3602);
		assertEquals(3602, FIRST.getTime());
		FIRST.update();
		assertEquals(3601, FIRST.getTime());
		FIRST.update();
		assertEquals(3600, FIRST.getTime());
		FIRST.update();
		assertEquals(3599, FIRST.getTime());
	}

}
