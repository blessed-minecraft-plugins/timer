package fr.blessedraccoon.minecraft.plugins.timer.commands;

import fr.blessedraccoon.minecraft.plugins.commandmanager.HelpCommandPattern;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;

public class CommandHelp extends HelpCommandPattern {

    public CommandHelp(TimerManager tm) {
        super(tm);
    }
    
    @Override
	public String getPermission() {
		return "timer.help";
	}
    
}
