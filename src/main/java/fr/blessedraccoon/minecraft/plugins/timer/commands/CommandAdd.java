package fr.blessedraccoon.minecraft.plugins.timer.commands;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.timer.timing.TimeOutOfBoundsException;

public class CommandAdd extends CommandPart {

    private TimerManager tm;

    public CommandAdd(TimerManager tm) {
        this.tm = tm;
    }

    private long lastTime = 0;
    private String timerName = "unknown";

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "adds or remove time to timer";
    }

    @Override
    public String getName() {
        return "add";
    }

    @Override
    public String getSyntax() {
        return "/timer add <Timer Name> <Seconds>";
    }

    @Override
    public void succeed(Player player) {
        String word = "added";
        if (this.lastTime < 0) {
            this.lastTime = -this.lastTime;
            word = "removed";
        }
        player.sendMessage(ChatColor.GREEN + "Successfully " + word + " " + this.lastTime + " seconds to the timer " + this.timerName);
    }

    @Override
    public boolean perform(Player player, String[] args) {
        try {
        	tm.getTimer(args[this.getDeepLevel()]).addTime(Long.parseLong(args[this.getDeepLevel() + 1]));
            this.lastTime = Long.parseLong(args[this.getDeepLevel() + 1]);
            this.timerName = args[this.getDeepLevel()];
            return true;
        } catch (TimeOutOfBoundsException e) {
            player.sendMessage(ChatColor.RED + "The time you added or removed exceeded the maximum supported by the timer format");
            return false;
        }
        catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + "The time you added or removed is not a number");
            return false;
        }
    }

	@Override
	public String getPermission() {
		return "timer.addtime";
	}
    
}
