package fr.blessedraccoon.minecraft.plugins.timer.timing;

import java.util.function.Function;

public class CountdownMethod implements Function<Long, Long> {

	@Override
	public Long apply(Long time) {
		return Math.max(time - 1, 0);
	}
	

}

