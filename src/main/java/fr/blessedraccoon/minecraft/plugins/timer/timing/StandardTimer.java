package fr.blessedraccoon.minecraft.plugins.timer.timing;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

public class StandardTimer {

    protected long time;
    
    private boolean running;
    private String name;
    private final List<TimeListener> triggers;
    
    private Function<Long, Long> countMethod;
    
    public static final Function<Long, Long> TIMER_METHOD = new TimerMethod();
    public static final Function<Long, Long> COUNTDOWN_METHOD = new CountdownMethod();

    public StandardTimer(String name) throws TimerInvalidNameException {
        if (name == null || name.equals("")) {
        	throw new TimerInvalidNameException("A timer cannot be null, or be named \"\".");
        }
        this.triggers = new ArrayList<>();
    	this.name = name;
    	this.time = 0;
        this.setRunning(false);
        this.setCountMethod(TIMER_METHOD);
    }

    public void update() {
    	if (this.isRunning()) {
    		this.time = this.countMethod.apply(this.time);
    		final long currTime = this.getTime();
            for (TimeListener trigger : triggers) {
                if (currTime == trigger.getTriggerTime()) {
                    trigger.trigger(this, currTime);
                }
            }
    	}
    }

    public void addTime(long time) throws TimeOutOfBoundsException{
        long currentTime = this.getTime() + time;
        if (currentTime < 0) {
            throw new TimeOutOfBoundsException("The time you want to add or remove is out of the timer bounds.");
        }
        this.time = currentTime;
    }

    public long getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
		if (! (o instanceof StandardTimer other)) return false;
		return this.getTime() == other.getTime();
    }

    @Override
    public String toString() {
    	long[] times = new long[5];
    	long time = this.time;
    	times[4] = time % 60; time /= 60;
    	times[3] = time % 60; time /= 60;
    	times[2] = time % 24; time /= 24;
    	times[1] = time % 365; time /= 365;
    	times[0] = time;
    	
    	StringBuilder sb = new StringBuilder();
		String[][] str = {
				{"year", "years"},
				{"day", "days"}
		};
		for(int i = 0 ; i < 2 ; i++) {
			if (times[i] != 0) {
				if (times[i] <= 1) {
					sb.append(times[i]).append(" " + str[i][0] + ", ");
				}
				else {
					sb.append(times[i]).append(" " + str[i][1] + ", ");
				}
			}
		}

		for(int i = 2 ; i < 5 ; i++) {
			if (times[i] < 10) sb.append("0");
			sb.append(times[i]);
			if (i != 4) sb.append(":");
		}
    	
        return sb.toString();
    }

    public void reset() {
        this.time = 0;
    }

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addTrigger(TimeListener tl) {
        this.triggers.add(tl);
    }

    public void removeTrigger(TimeListener tl) {
        this.triggers.remove(tl);
    }

    public void removeTrigger(int index) {
        this.triggers.remove(index);
    }

    public List<TimeListener> getTriggers() {
        return triggers;
    }

	public Function<Long, Long> getCountMethod() {
		return countMethod;
	}

	public void setCountMethod(Function<Long, Long> countMethod) {
		this.countMethod = Objects.requireNonNullElse(countMethod, TIMER_METHOD);
	}


}
