package fr.blessedraccoon.minecraft.plugins.timer.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;

public class CommandStop extends CommandPart {

    private TimerManager tm;

    public CommandStop(TimerManager tm) {
        this.tm = tm;
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "stops the timer";
    }

    @Override
    public String getName() {
        return "stop";
    }

    @Override
    public String getSyntax() {
        return "/timer stop <Timer>";
    }

    @Override
    public void succeed(Player player) {
        player.sendMessage("successfully stopped the timer.");
    }

    @Override
    public boolean perform(Player player, String[] args) {
    	tm.getTimer(args[this.getDeepLevel()]).setRunning(false);
        return true;
    }
    
    @Override
	public String getPermission() {
		return "timer.stop";
	}
    
}

