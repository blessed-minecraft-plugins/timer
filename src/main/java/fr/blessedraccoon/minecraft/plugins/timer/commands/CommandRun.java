package fr.blessedraccoon.minecraft.plugins.timer.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandValuable;
import fr.blessedraccoon.minecraft.plugins.timer.timing.StandardTimer;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;

public class CommandRun extends CommandPart implements CommandValuable {

    private TimerManager tm;

    public CommandRun(TimerManager tm) {
        this.tm = tm;
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "runs the timer";
    }

    @Override
    public String getName() {
        return "run";
    }

    @Override
    public String getSyntax() {
        return "/timer run <Timer Name>";
    }

    @Override
    public void succeed(Player player) {
        player.sendMessage("successfully started the timer.");
    }

    @Override
    public boolean perform(Player player, String[] args) {
    	tm.getTimer(args[this.getDeepLevel()]).setRunning(true);
        return true;
    }

    @Override
    public String getValue() {
    	StringBuilder sb = new StringBuilder();
    	for(StandardTimer timer : tm.getTimers()) {
    		sb.append(timer.getName()).append(" : ").append(timer.isRunning());
    	}
        return sb.toString();
    }
    
    @Override
	public String getPermission() {
		return "timer.run";
	}
    
}
