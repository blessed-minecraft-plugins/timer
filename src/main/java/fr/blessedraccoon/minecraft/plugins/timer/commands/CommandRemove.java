package fr.blessedraccoon.minecraft.plugins.timer.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerInvalidNameException;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;
import net.md_5.bungee.api.ChatColor;

public class CommandRemove extends CommandPart {

	private TimerManager tm;

	public CommandRemove(TimerManager tm) {
		this.tm = tm;
	}

	@Override
	public int getDeepLevel() {
		return 1;
	}

	@Override
	public String getDescription() {
		return "Removes the timer with the given name";
	}

	@Override
	public String getName() {
		return "remove";
	}

	@Override
	public String getPermission() {
		return "timer.remove";
	}

	@Override
	public String getSyntax() {
		return "/timer remove <Name>";
	}

	@Override
	public boolean perform(Player player, String[] args) {
		try {
			tm.removeTimer(args[this.getDeepLevel()]);
		} catch (TimerInvalidNameException e) {
			player.sendMessage(ChatColor.RED + e.getMessage());
			return false;
		}
		return true;
	}

}
