package fr.blessedraccoon.minecraft.plugins.timer.timing;

import java.util.ArrayList;
import java.util.List;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandManager;
import fr.blessedraccoon.minecraft.plugins.timer.commands.*;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import org.bukkit.plugin.Plugin;

public class TimerManager extends CommandManager implements Runnable {

    private final List<StandardTimer> timers;
    private StandardTimer visible;
    private long speed;
    public static final int NORMAL_SPEED = 20;

    private int taskId;

    public TimerManager(Plugin plugin) {
        this.timers = new ArrayList<>();
        this.speed = NORMAL_SPEED;
        if (plugin != null) { // To be able to execute tests without a plugin run
            this.taskId = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, this, 0, this.speed);
        }
        
        this.setVisible(null);
        this.addSubcommand(new CommandAdd(this));
        this.addSubcommand(new CommandDisplay(this));
        this.addSubcommand(new CommandHide(this));
        this.addSubcommand(new CommandReset(this));
        this.addSubcommand(new CommandRun(this));
        this.addSubcommand(new CommandStop(this));
        this.addSubcommand(new CommandList(this));
        this.addSubcommand(new CommandCreate(this));
        this.addSubcommand(new CommandRemove(this));
        this.addSubcommand(new CommandSpeed(this));

        this.addSubcommand(new CommandRecap(this));
        this.addSubcommand(new CommandHelp(this));
    }

    public void setSpeed(long speed) {
        this.speed = (this.speed <= 0) ? NORMAL_SPEED : speed;
        if (taskId != Integer.MIN_VALUE) {
            this.removeSpeed();
        }
    }

    public void removeSpeed() {
        Bukkit.getServer().getScheduler().cancelTask(taskId);
    }

    public long getSpeed() {
        return speed;
    }
    
    public void removeTimer(String name) throws TimerInvalidNameException {
    	StandardTimer timer = this.getTimer(name);
    	if (timer == null) {
    		throw new TimerInvalidNameException("No timer has been found to remove for the name " + name);
    	}
    	this.timers.remove(timer);
    	// If the deleted timer is on currently visible, display no timer anymore
    	if (timer == this.visible) {
    		this.visible = null;
    	}
    }
    
    public void createTimer(String name) throws TimerInvalidNameException {
    	if (this.getTimer(name) != null) {
    		throw new TimerInvalidNameException("Another timer has the name " + name);
    	}
    	this.timers.add(new StandardTimer(name));
    }
    
    public List<StandardTimer> getTimers() {
    	return this.timers;
    }
    
    public StandardTimer getTimer(String name) {
        var it = this.timers.iterator();
    	while(it.hasNext()) {
            var timer = it.next();
    		if (timer.getName().equals(name)) {
    			return timer;
    		}
    	}
    	return null;
    }

    public void update() {
        var it = this.timers.iterator(); // Using this to avoid ConcurrentModificationException
        while (it.hasNext()) {
    		it.next().update();
    	}
    }

    @Override
    public int getDeepLevel() {
        return 0;
    }

    @Override
    public String getDescription() {
        return "The command that interacts with Timers";
    }

    @Override
    public String getName() {
        return "timer";
    }

    @Override
    public String getSyntax() {
        return "/timer";
    }

    @Override
    public boolean perform(Player arg0, String[] arg1) {return false;}

	@Override
	public String getPermission() {
		return "timer";
	}

	public StandardTimer getVisible() {
		return visible;
	}

	public void setVisible(StandardTimer visible) {
		this.visible = visible;
	}

    @Override
    public void run() {
        this.update();

        StandardTimer timer = this.getVisible();
        // If a timer is visible
        if (timer != null) {
            var players = Bukkit.getOnlinePlayers();
            for (var player : players) {
                player.sendActionBar(Component.text(timer.toString()));
            }
        }
    }


}
