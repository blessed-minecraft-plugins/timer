package fr.blessedraccoon.minecraft.plugins.timer.commands;

import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;
import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandValuable;

public class CommandSpeed extends CommandPart implements CommandValuable {

    private TimerManager tm;

    public CommandSpeed(TimerManager tm) {
        this.tm = tm;
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "sets the speed of the timers";
    }

    @Override
    public String getName() {
        return "speed";
    }

    @Override
    public String getSyntax() {
        return "/timer speed <Ticks per update>";
    }


    @Override
    public boolean perform(Player player, String[] args) {
    	long coeff = Long.parseLong(args[this.getDeepLevel()]);
    	if (coeff <= 0) {
    		coeff = TimerManager.NORMAL_SPEED;
    	}
    	tm.setSpeed(coeff);
    	return true;
    }

    @Override
    public String getValue() {
        return "" + tm.getSpeed() + " ticks per update";
    }
    
    @Override
	public String getPermission() {
		return "timer.speed";
	}
    
}
