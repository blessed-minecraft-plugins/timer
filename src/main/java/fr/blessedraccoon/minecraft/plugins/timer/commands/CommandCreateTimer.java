package fr.blessedraccoon.minecraft.plugins.timer.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerInvalidNameException;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import net.md_5.bungee.api.ChatColor;

public class CommandCreateTimer extends CommandPart {

	private TimerManager tm;

	public CommandCreateTimer(TimerManager tm) {
		this.tm = tm;
	}

	@Override
	public int getDeepLevel() {
		return 2;
	}

	@Override
	public String getDescription() {
		return "Creates a timer with the given name";
	}

	@Override
	public String getName() {
		return "timer";
	}

	@Override
	public String getPermission() {
		return "timer.create";
	}

	@Override
	public String getSyntax() {
		return "/timer create timer <Name>";
	}

	@Override
	public boolean perform(Player player, String[] args) {
		try {
			tm.createTimer(args[this.getDeepLevel()]);
		} catch (TimerInvalidNameException e) {
			player.sendMessage(ChatColor.RED + e.getMessage());
			return false;
		}
		return true;
	}

}
