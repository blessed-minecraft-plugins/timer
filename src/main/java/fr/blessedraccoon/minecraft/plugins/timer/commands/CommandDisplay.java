package fr.blessedraccoon.minecraft.plugins.timer.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandValuable;
import fr.blessedraccoon.minecraft.plugins.timer.timing.StandardTimer;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;
import net.md_5.bungee.api.ChatColor;

public class CommandDisplay extends CommandPart implements CommandValuable {

    private TimerManager tm;

    public CommandDisplay(TimerManager tm) {
        this.tm = tm;
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "shows the timer";
    }

    @Override
    public String getName() {
        return "display";
    }

    @Override
    public String getSyntax() {
        return "/timer display <Timer Name>";
    }

    @Override
    public void succeed(Player player) {
        player.sendMessage(ChatColor.GREEN + "successfully displayed the timer.");
    }

    @Override
    public boolean perform(Player player, String[] args) {
    	StandardTimer timer = tm.getTimer(args[this.getDeepLevel()]);
    	if (timer == null) return false;
    	tm.setVisible(timer);
        return true;
    }

    @Override
    public String getValue() {
        return "" + tm.getVisible().getName();
    }
    
    @Override
	public String getPermission() {
		return "timer.display";
	}
    
}
