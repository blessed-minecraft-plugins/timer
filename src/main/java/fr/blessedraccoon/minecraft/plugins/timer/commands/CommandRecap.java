package fr.blessedraccoon.minecraft.plugins.timer.commands;

import fr.blessedraccoon.minecraft.plugins.commandmanager.RecapCommandValue;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;

public class CommandRecap extends RecapCommandValue {

    public CommandRecap(TimerManager tm) {
        super(tm);
    }
    
    @Override
	public String getPermission() {
		return "timer.recap";
	}
    
}
