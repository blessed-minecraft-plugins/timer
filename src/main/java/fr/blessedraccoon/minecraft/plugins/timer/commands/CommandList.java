package fr.blessedraccoon.minecraft.plugins.timer.commands;

import java.util.List;

import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import fr.blessedraccoon.minecraft.plugins.timer.timing.StandardTimer;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;
import net.md_5.bungee.api.ChatColor;

public class CommandList extends CommandPart {

	private TimerManager tm;

	public CommandList(TimerManager tm) {
		this.tm = tm;
	}

	@Override
	public int getDeepLevel() {
		return 1;
	}

	@Override
	public String getDescription() {
		return "shows the lists of current timers";
	}

	@Override
	public String getName() {
		return "list";
	}

	@Override
	public String getPermission() {
		return "timer.list";
	}

	@Override
	public String getSyntax() {
		return "/timer list";
	}
	
	@Override
	public void fail(Player p) {
		p.sendMessage(ChatColor.RED + "No timer has been found.");
	}

	@Override
	public boolean perform(Player player, String[] args) {
		List<StandardTimer> timers = tm.getTimers();
		if (timers.size() == 0) return false;
		for(StandardTimer timer : timers) {
			final String running = (timer.isRunning()) ? ChatColor.GREEN + "running" : ChatColor.RED + "stopped";
			final String displayed = (tm.getVisible() == timer) ? ChatColor.GOLD + "displayed" : "";
			
			String count = "unknown";
			if (timer.getCountMethod() == StandardTimer.TIMER_METHOD) count = "timer";
			if (timer.getCountMethod() == StandardTimer.COUNTDOWN_METHOD) count = "countdown";
			count = ChatColor.AQUA + count;
			
			player.sendMessage(String.format("%s : %s, %s %s %s", timer.getName(), timer,
					running, count, displayed));
		}
		return true;
	}

}
