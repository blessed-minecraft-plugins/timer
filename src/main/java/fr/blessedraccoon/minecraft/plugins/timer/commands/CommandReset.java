package fr.blessedraccoon.minecraft.plugins.timer.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;

public class CommandReset extends CommandPart {

    private TimerManager tm;

    public CommandReset(TimerManager tm) {
        this.tm = tm;
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "resets the timer";
    }

    @Override
    public String getName() {
        return "reset";
    }

    @Override
    public String getSyntax() {
        return "/timer reset <Timer Name>";
    }

    @Override
    public void succeed(Player player) {
        player.sendMessage("successfully reset the timer.");
    }

    @Override
    public boolean perform(Player player, String[] args) {
    	tm.getTimer(args[this.getDeepLevel()]).reset();
        return true;
    }
    
    @Override
	public String getPermission() {
		return "timer.reset";
	}
    
}

