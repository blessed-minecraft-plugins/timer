package fr.blessedraccoon.minecraft.plugins.timer.timing;

public interface TimeListener {
    int getTriggerTime();
    void trigger(StandardTimer tm, long time);
}