package fr.blessedraccoon.minecraft.plugins.timer.timing;

public class TimeOutOfBoundsException extends Exception {

    public TimeOutOfBoundsException(String message) {
        super(message);
    }
}
