package fr.blessedraccoon.minecraft.plugins.timer;

import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;
import org.bukkit.plugin.java.JavaPlugin;

public class BlessedTimer extends JavaPlugin {

    @Override
    public void onEnable() {
        var timerControl = new TimerManager(this);
        var command = getCommand("timer");
        if (command != null) {
            command.setTabCompleter(timerControl);
            command.setExecutor(timerControl);
        }
        else {
            getLogger().severe("A problem occurred during the timer plugin loading");
            return;
        }

        getLogger().info("The timer plugin has run successfully");

    }

}
