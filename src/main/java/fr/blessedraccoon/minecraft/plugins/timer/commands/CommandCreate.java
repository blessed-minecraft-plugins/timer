package fr.blessedraccoon.minecraft.plugins.timer.commands;

import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;
import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;

public class CommandCreate extends CommandPart {
	
	public CommandCreate(TimerManager tm) {
		this.addSubcommand(new CommandCreateTimer(tm));
		this.addSubcommand(new CommandCreateCountdown(tm));
	}

	@Override
	public int getDeepLevel() {
		return 1;
	}

	@Override
	public String getDescription() {
		return "Creates a timer with the given name";
	}

	@Override
	public String getName() {
		return "create";
	}

	@Override
	public String getPermission() {
		return "timer.create";
	}

	@Override
	public String getSyntax() {
		return "/timer create <timer | countdown> <Name>";
	}

	@Override
	public boolean perform(Player player, String[] args) {
		return false;
	}

}
