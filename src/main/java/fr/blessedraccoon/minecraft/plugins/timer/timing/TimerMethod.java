package fr.blessedraccoon.minecraft.plugins.timer.timing;

import java.util.function.Function;

public class TimerMethod implements Function<Long, Long> {

	@Override
	public Long apply(Long time) {
		return time + 1;
	}
	

}
