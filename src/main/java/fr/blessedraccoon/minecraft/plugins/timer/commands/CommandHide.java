package fr.blessedraccoon.minecraft.plugins.timer.commands;

import org.bukkit.entity.Player;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandPart;
import fr.blessedraccoon.minecraft.plugins.timer.timing.TimerManager;
import net.md_5.bungee.api.ChatColor;

public class CommandHide extends CommandPart {

    private TimerManager tm;

    public CommandHide(TimerManager tm) {
        this.tm = tm;
    }

    @Override
    public int getDeepLevel() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "hides the timer";
    }

    @Override
    public String getName() {
        return "hide";
    }

    @Override
    public String getSyntax() {
        return "/timer hide";
    }

    @Override
    public void succeed(Player player) {
        player.sendMessage(ChatColor.GREEN + "successfully hid the timer.");
    }

    @Override
    public boolean perform(Player player, String[] args) {
    	tm.setVisible(null);
        return true;
    }
    
    @Override
	public String getPermission() {
		return "timer.hide";
	}
    
}

