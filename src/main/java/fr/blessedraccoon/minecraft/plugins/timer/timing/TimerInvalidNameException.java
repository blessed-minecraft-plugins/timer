package fr.blessedraccoon.minecraft.plugins.timer.timing;

public class TimerInvalidNameException extends Exception {

	public TimerInvalidNameException(String content) {
		super(content);
	}
	
}
